import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
})
class DepartmentsContainer extends Component {
  constructor(props){
    super(props)
    this.state = {
      departments: [],
      anchorEl: null,
    }
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  
  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  
  componentDidMount() {
    axios.get('http://localhost:3001/api/v1/departments.json')
      .then(response => {
        this.setState({
          departments: response.data
        })
      })
      .catch(error => console.log(error))
  }
  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;

    return (
      <div className="departments-container">
        <Button 
          className={classes.menuButton} 
          color="inherit" 
          onClick={this.handleClick}
        >
            Departments
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
              <MenuItem onClick={() => {
                this.props.handleSelect({id: 0, name: 'All Products'});
                this.handleClose();
              }}
              >
              All
              </MenuItem>
          {this.state.departments.map(department => {
            return (
              <MenuItem 
                onClick={() => {
                  this.props.handleSelect(department);
                  this.handleClose();
                }}
                key={'DepartmentsMenu' + department.id}
              >
                {department.name}
              </MenuItem>
            )
          })}
        </Menu>
      </div>
    )
  }
}

DepartmentsContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DepartmentsContainer);
