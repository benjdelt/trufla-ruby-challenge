import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    objectFit: 'cover',
  },
  newprice: {
    display: 'inline',
  },
  oldprice: {
    display: 'inline',
    marginLeft: '40px',
    textDecoration: 'line-through',
  },
};

const Product = props => {
  let newPrice = props.data.product.price_cents;
  props.data.promotions.forEach(promotion => {
    newPrice = newPrice - (newPrice * promotion.discount / 100);
  });
  const { classes } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Product Name"
          className={classes.media}
          height="300"
          image={props.data.product.image_link}
          title="Product Name"
        />
        <CardContent>
          <Typography gutterBottom variant="h5">
            {props.data.product.name}
          </Typography>
          {props.data.promotions.map(promotion => {
            return (
              <Typography gutterBottom variant="h6" color="secondary" key={'ProductPromotionName' + promotion.id}>
                {promotion.name}
              </Typography>
            )
          })}
          <Typography component="p">
            {props.data.product.description}
          </Typography>
          {props.data.promotions.length ? (
            <div key={'ProductPromotionPrice' + props.data.product.id}>
              <Typography 
                className={classes.newprice} 
                gutterBottom variant="h6" 
                color="secondary"
              >
                $ {(newPrice / 100).toFixed(2)}
              </Typography>
              <Typography className={classes.oldprice} gutterBottom variant="h6">
                $ {props.data.product.price_cents / 100}
              </Typography>
            </div>
          ) : (
            <Typography gutterBottom variant="h6" >
              $ {props.data.product.price_cents / 100}
            </Typography>
          )
        }
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

Product.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Product);
