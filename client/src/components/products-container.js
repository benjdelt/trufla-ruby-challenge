import React, { Component } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroller';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

import Product from './product';
import { Typography } from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  product: {
    margin: 0, 
    width: '100%',
  },
  progress: {
    margin: theme.spacing.unit * 4,
  },
});

class ProductsContainer extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading: true,
      page: 1,
      hasMore: true,
      products: [],
      filteredProducts: [],
    }
  }

  fetchProducts = () => {
    if (this.props.searchResults.length || this.props.selectedDept || this.props.selectedProm) {
      return;
    }
    return axios.get(`http://localhost:3001/api/v1/products?q=${this.state.page}`)
      .then(response => {
        if (response.data.length) {
          let newPage = this.state.page + 1;
          const newProducts = this.state.products.concat(response.data);
          const newFilteredProducts = this.state.filteredProducts.concat(response.data);
          this.setState({
            loading: false,
            page: newPage,
            products: newProducts,
            filteredProducts: newFilteredProducts
          })
        } else {
          this.setState({hasMore: false});
        }
      })
      .catch(error => console.log(error))
  };

  filterByDept(products, depId){
    if (!depId) {
      return products;
    }
    const filteredProducts = products.filter(product => {
      return product.product.department_id === depId;
    })
    return filteredProducts;
  }

  filterByProm(products, promId) {
    let productPromIds = [];
    const filteredProducts = products.filter(product => {
      productPromIds = product.promotions.map(promotion => promotion.id);
      return productPromIds.includes(promId);
    })
    return filteredProducts;
  }

  componentWillReceiveProps(nextProps) {

    // Display Search results

    if(nextProps.searchResults === 'No result to show') {
      this.setState({filteredProducts: []});      
    } else if(nextProps.searchResults.length) {
      this.setState({filteredProducts: nextProps.searchResults});
    } else {

      // Filter Products according to the selected department or promotion
      
      let filteredByProm = this.state.products;
      const filteredByDept = this.filterByDept(this.state.products, nextProps.selectedDept);
      if (nextProps.selectedProm) {
        filteredByProm = this.filterByProm(this.state.products, nextProps.selectedProm);
      }
      const filteredProducts = filteredByDept.filter(product => {
        return filteredByProm.includes(product);
      })
      this.setState({filteredProducts: filteredProducts});
    }

  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container className={classes.root} spacing={0}>
        <InfiniteScroll
          pageStart={0}
          loadMore={this.fetchProducts}
          hasMore={this.state.hasMore}
          loader={ 
            <CircularProgress 
              className={classes.progress}
              key={'ProductLoader' + this.state.page}
              /> 
          }
        >
          <Grid item xs={12}>
            <Grid 
              container 
              className={classes.product} 
              justify="center" 
              spacing={32}
            >
              {this.state.filteredProducts.length ? (
                this.state.filteredProducts.map(product => {
                  return (
                    <Grid item key={'ProductContainer' + product.product.id + this.state.page}>
                      <Product data={product} />
                    </Grid>  
                  )
                })
              ) : (
                this.state.loading ? (
                  <div></div>
                ) : (
                  <Typography gutterBottom variant="h5">
                    No result to show
                  </Typography>
                )
              )}
            </Grid>
          </Grid>
        </InfiniteScroll>
      </Grid>
    )
  }
}

ProductsContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductsContainer);
