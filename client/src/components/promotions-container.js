import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
})
class DepartmentsContainer extends Component {
  constructor(props){
    super(props)
    this.state = {
      promotions: [],
      anchorEl: null,
    }
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  componentDidMount() {
    axios.get('http://localhost:3001/api/v1/promotions.json')
      .then(response => {
        this.setState({
          promotions: response.data
        })
      })
      .catch(error => console.log(error))
  }
  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;

    return (
      <div className="promotions-container">
        <Button 
            className={classes.menuButton} 
            variant="contained" 
            color="secondary"
            onClick={this.handleClick}
          >
            Promotions
          </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {this.state.promotions.map(promotion => {
            return (
              <MenuItem 
                onClick={() => {
                  this.props.handleSelect(null, promotion);
                  this.handleClose();
                }}
                key={'PromotionsMenu' + promotion.id}
              >
                {promotion.name}
              </MenuItem>
            )
          })}
        </Menu>
      </div>
    )
  }
}

DepartmentsContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DepartmentsContainer);
