import React, { Component } from 'react';
import './App.css';
import Navbar from './components/navbar';
import ProductsContainer from './components/products-container';
import { Typography } from '@material-ui/core';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDept: 0,
      departmentName: 'All Products',
      selectedProm: 0,
      searchResults: [],
    }
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect = (dep, prom) => {
    console.log()
    if (dep && (dep.id || dep.id === 0)) {
      this.setState({ 
        selectedDept: dep.id,
        departmentName: dep.name,
        selectedProm: 0,
        searchResults: [],
      });
    }
    if (prom && (prom.id || prom.id === 0)) {
      this.setState({ 
        selectedProm: prom.id,
        searchResults: [],
      });
    }
  }

  handleSearch = (results) => {
    if (results.length) {
      this.setState({
        searchResults: results,
        selectedDept: 0,
        departmentName: 'All Products',
        selectedProm: 0,
      })
    } else {
      this.setState({
        searchResults: 'No result to show',
        selectedDept: 0,
        departmentName: 'All Products',
        selectedProm: 0,
      })
    }
  }

  render() {
    return (
      <div className="App">
        <Navbar handleSelect={this.handleSelect} handleSearch={this.handleSearch}/>
        <Typography gutterBottom variant="h6">
          {this.state.departmentName}
        </Typography>
        <ProductsContainer 
          selectedDept={this.state.selectedDept} 
          selectedProm={this.state.selectedProm}
          searchResults={this.state.searchResults}
        />
      </div>
    );
  }
}

export default App;
