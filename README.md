# README

This is my submission to the [Ruby Challenge](https://gitlab.com/trufla/ruby-challenge) that Trufla assigned to me. It's a prototype for a basic e-commerce React app connected to a Rails api. I implemented every core feature. I ran into several challenges, the biggest one was probably to have the filter and search components working together. Also, it was only when I tried to implement the pagination that I realized that Material Design (which I had already used on all my components) doesn't cover it as they don't consider it to be mobile friendly. So I decided to implement the pagination on the back-end and an infinite scroll on the front-end. It was a very interesting project to complete, and I learned a lot by doing it.

The ERD and the wireframe used during the planning of this project can be found in the `docs/` folder.

## Screenshots

![Filters](./docs/02.gif)

*Department and Promotion filters*

![Search](./docs/03.gif)

*Search*

## Getting Started

- Clone this repository on your local machine and cd into it
- Install the back-end dependencies 
  ```
  bundle install
  ```
- Run the migrations
  ```
  rails db:migrate
  ```
- Run the seeds
  ```
  rails db:seed
  ```
- On another terminal window, cd into the `client` folder and install the front-end dependencies
  ```
  cd client
  npm install
  ```
- On the first window, launch the server api on Port 3001
  ```
  rails s -p 3001
  ```
- On the second window, run the front-end app
  ```
  npm start
  ``` 
- On your browser, go to `http://localhost:3000`

## Dependencies

- Ruby (v. 2.3)
- Ruby on Rails (v. 5.1)
  - Rack CORS
  - Kaminari
- React
  - Axios
  - Material Design Core
  - Material Desing Icons
  - Prop-types
  - React Infinite Scroller
