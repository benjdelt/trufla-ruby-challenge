module Api::V1
  class ProductsController < ApplicationController
    def index
      @products = Product.all.page(params[:q]).per(15)
      @products = @products.map {|product| {product: product, promotions: product.promotions}}
      render json: @products
    end

    def search 
      query = params[:q].downcase
      @products = Product.where("lower(name) LIKE ?", "%#{query}%")
      @products = @products.map {|product| {product: product, promotions: product.promotions}}
      render json: @products
    end

  end
end
