module Api::V1
  class DepartmentsController < ApplicationController
    def index
      @departments = Department.all
      render json: @departments
    end
  end
end
