class Product < ApplicationRecord
  belongs_to :department
  has_many :product_promotions, dependent: :destroy
  has_many :promotions, through: :product_promotions
end
