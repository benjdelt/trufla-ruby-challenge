# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def create_product! department 
  department.products.create!({
    name: Faker::Commerce.unique.product_name,
    description: Faker::Lorem.sentence,
    image_link: 'https://picsum.photos/350/?random',
    price_cents: Faker::Commerce.price * 100,
    quantity: rand(20)
  })
end

puts 'Seeding...'

Department.destroy_all

department_1 = Department.create! name: Faker::Commerce.unique.department
department_2 = Department.create! name: Faker::Commerce.unique.department
department_3 = Department.create! name: Faker::Commerce.unique.department
department_4 = Department.create! name: Faker::Commerce.unique.department

ProductPromotion.destroy_all

Product.destroy_all 

create_product!(department_2)
create_product!(department_2)
create_product!(department_2)
product_1 = create_product!(department_1)
product_2 = create_product!(department_1)
create_product!(department_3)
create_product!(department_3)
create_product!(department_4)
product_3 = create_product!(department_1)
product_4 = create_product!(department_2)
create_product!(department_4)
create_product!(department_4)
create_product!(department_1)
create_product!(department_3)
create_product!(department_4)
create_product!(department_4)
create_product!(department_4)
product_5 = create_product!(department_2)
create_product!(department_2)
create_product!(department_2)
create_product!(department_2)
product_6 = create_product!(department_2)
product_7 = create_product!(department_2)
create_product!(department_1)
create_product!(department_1)
product_8 = create_product!(department_3)
product_9 = create_product!(department_3)
create_product!(department_1)
create_product!(department_2)
create_product!(department_2)
create_product!(department_2)
product_10 = create_product!(department_4)
product_11 = create_product!(department_4)
create_product!(department_1)
create_product!(department_1)
create_product!(department_1)
create_product!(department_3)
create_product!(department_4)
create_product!(department_4)


Promotion.destroy_all

promotion_1 = Promotion.create!({
  name: Faker::Commerce.unique.promotion_code,
  discount: rand(1..14) * 5,
  })
promotion_2 = Promotion.create!({
  name: Faker::Commerce.unique.promotion_code,
  discount: rand(1..14) * 5,
})
promotion_3 = Promotion.create!({
  name: Faker::Commerce.unique.promotion_code,
  discount: rand(1..14) * 5,
})
promotion_4 = Promotion.create!({
  name: Faker::Commerce.unique.promotion_code,
  discount: rand(1..14) * 5,
})

product_1.promotions << promotion_1
product_2.promotions << promotion_1
product_3.promotions << promotion_1
product_4.promotions << promotion_1
product_4.promotions << promotion_2
product_5.promotions << promotion_2
product_6.promotions << promotion_2
product_7.promotions << promotion_3
product_8.promotions << promotion_2
product_8.promotions << promotion_3
product_9.promotions << promotion_3
product_10.promotions << promotion_4
product_11.promotions << promotion_4
product_11.promotions << promotion_3

puts 'Done'
